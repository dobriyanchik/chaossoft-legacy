﻿namespace TsaToolbox.Models
{
    public class Settings
    {
        public int SaveChartWidth { get; set; }

        public int SaveChartHeight { get; set; }

        public int PreviewWindowWidth { get; set; }

        public int PreviewWindowHeight { get; set; }

    }
}
